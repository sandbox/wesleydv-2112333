<?php

/**
 * @file
 * Capture plus admin settings.
 */

/**
 * Settings form.
 */
function capture_plus_settings_form(&$form_state) {
  // Give a warning to avoid key confusion issues.
  $message1 = t("Make sure the key you enter here is from the 'WebClient' type, by default Postcode Anywhere provides you with a 'WebService' key.");
  $message2 = t("If you don't have a 'WebClient' key yet, create a new Capture+ key on the Postcode Anywhere website and select 'Capture+ for website'.");
  drupal_set_message($message1, 'warning', FALSE);
  drupal_set_message($message2, 'warning', FALSE);

  $form['capture_plus_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Capture+ key'),
    '#default_value' => variable_get('capture_plus_key', ''),
    '#description' => t('This is the key will be used as the default capture_plus_key value on all Capture plus fields.'),
  );

  $form['capture_plus_minified'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use minified version'),
    '#default_value' => variable_get('capture_plus_minified', FALSE),
    '#description' => t('Tick to use the minified version of the Capture+ JavaScript'),
  );

  $form['capture_plus_stub'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use stub'),
    '#default_value' => variable_get('capture_plus_stub', FALSE),
    '#description' => t('Tick to use the locally hosted stub version of the Capture+ JavaScript. This will not make any calls to PCA.'),
  );

  return system_settings_form($form);
}
