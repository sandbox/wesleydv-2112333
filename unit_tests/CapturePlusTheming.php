<?php
/**
 * @file
 * Test theming functions for capture_plus
 */

/**
 * PHPUnit CapturePlusTheming
 */
class CapturePlusTheming extends PHPUnit_Framework_TestCase {

  /**
   * Set up function to include required file.
   *
   * All test can be done without bootstrapping Drupal.
   */
  public function setUp() {
    include_once __DIR__ . '/../capture_plus.theme.inc';
  }

  /**
   * Tests capture_plus_search_box_get_size().
   *
   * @dataProvider providerCapturePlusSearchBoxGetSize
   */
  public function testCapturePlusSearchBoxGetSize($element, $result) {
    $this->assertEquals(capture_plus_search_box_get_size($element), $result);
  }

  /**
   * Data provider for testCapturePlusSearchBoxGetSize.
   */
  public static function providerCapturePlusSearchBoxGetSize() {
    $test1 = array(
      'element' => array(),
      'result' => '',
    );
    $test2 = array(
      'element' => array('#size' => '10'),
      'result' => ' size="10"',
    );

    return array($test1, $test2);
  }

  /**
   * Tests capture_plus_search_box_get_id().
   *
   * @dataProvider providerCapturePlusSearchBoxGetID
   */
  public function testCapturePlusSearchBoxGetID($element, $result) {
    $this->assertEquals(capture_plus_search_box_get_id($element), $result);
  }

  /**
   * Data provider for testCapturePlusSearchBoxGetID.
   */
  public static function providerCapturePlusSearchBoxGetID() {
    $test1 = array(
      'element' => array(),
      'result' => '',
    );
    $test2 = array(
      'element' => array('#capture_plus_id' => 'test'),
      'result' => 'test',
    );
    $test3 = array(
      'element' => array('#id' => 'test'),
      'result' => 'test-search',
    );
    $test4 = array(
      'element' => array('#id' => 'test', '#capture_plus_id' => ''),
      'result' => 'test-search',
    );

    return array($test1, $test2, $test3, $test4);
  }

  /**
   * Tests capture_plus_search_box_get_prefix().
   *
   * @dataProvider providerCapturePlusSearchBoxGetPrefix
   */
  public function testCapturePlusSearchBoxGetPrefix($element, $result) {
    $this->assertEquals(capture_plus_search_box_get_prefix($element), $result);
  }

  /**
   * Data provider for testCapturePlusSearchBoxGetPrefix.
   */
  public static function providerCapturePlusSearchBoxGetPrefix() {
    $test1 = array(
      'element' => array(),
      'result' => '',
    );
    $test2 = array(
      'element' => array('#field_prefix' => 'PREFIX'),
      'result' => '<span class="field-prefix">PREFIX</span> ',
    );
    $test3 = array(
      'element' => array('#field_prefix' => ''),
      'result' => '',
    );

    return array($test1, $test2, $test3);
  }

  /**
   * Tests capture_plus_search_box_get_suffix().
   *
   * @dataProvider providerCapturePlusSearchBoxGetSuffix
   */
  public function testCapturePlusSearchBoxGetSuffix($element, $result) {
    $this->assertEquals(capture_plus_search_box_get_suffix($element), $result);
  }

  /**
   * Data provider for testCapturePlusSearchBoxGetSuffix.
   */
  public static function providerCapturePlusSearchBoxGetSuffix() {
    $test1 = array(
      'element' => array(),
      'result' => '',
    );
    $test2 = array(
      'element' => array('#field_suffix' => 'SUFFIX'),
      'result' => ' <span class="field-suffix">SUFFIX</span>',
    );
    $test3 = array(
      'element' => array('#field_suffix' => ''),
      'result' => '',
    );

    return array($test1, $test2, $test3);
  }

  /**
   * Tests capture_plus_search_box_get_element().
   *
   * @dataProvider providerCapturePlusSearchBoxGetElement
   */
  public function testCapturePlusSearchBoxGetElement($element, $id, $size, $value, $attributes, $result) {
    $this->assertEquals(capture_plus_search_box_get_element($element, $id, $size, $value, $attributes), $result);
  }

  /**
   * Data provider for testCapturePlusSearchBoxGetElement.
   */
  public static function providerCapturePlusSearchBoxGetElement() {
    $test1 = array(
      'element' => array(),
      'id' => '',
      'size' => '',
      'value' => '',
      'attributes' => '',
      'result' => '',
    );
    $test2 = array(
      'element' => array('#name' => 'NAME'),
      'id' => '',
      'size' => '',
      'value' => '',
      'attributes' => '',
      'result' => '<input type="text" name="NAME-search" id="" value="" />',
    );
    $test3 = array(
      'element' => array('#name' => 'NAME'),
      'id' => 'ID',
      'size' => '',
      'value' => '',
      'attributes' => '',
      'result' => '<input type="text" name="NAME-search" id="ID" value="" />',
    );
    $test4 = array(
      'element' => array('#name' => 'NAME'),
      'id' => 'ID',
      'size' => ' size=10',
      'value' => '',
      'attributes' => '',
      'result' => '<input type="text" name="NAME-search" id="ID" size=10 value="" />',
    );
    $test5 = array(
      'element' => array('#name' => 'NAME'),
      'id' => 'ID',
      'size' => ' size=10',
      'value' => 'VALUE',
      'attributes' => '',
      'result' => '<input type="text" name="NAME-search" id="ID" size=10 value="VALUE" />',
    );
    $test6 = array(
      'element' => array('#name' => 'NAME'),
      'id' => 'ID',
      'size' => ' size=10',
      'value' => 'VALUE',
      'attributes' => ' class="CLASS"',
      'result' => '<input type="text" name="NAME-search" id="ID" size=10 value="VALUE" class="CLASS" />',
    );

    return array($test1, $test2, $test3, $test4, $test5, $test6);
  }
}
