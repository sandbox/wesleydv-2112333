/**
 * This serves both as an example on how to override the default js and as an
 * easy way to show developers what is wrong with their setup.
 */
(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.capturePlusExample = function(context) {
    var search_box = $('input.search-box.capture-plus', context);

    // This disables the default error handling.
    search_box.unbind('capturePlusError.default');

    // New error handling.
    search_box.bind('capturePlusError.example', function(event, uid, response) {
      console.log('Capture Plus Example Error');
      console.log(response);

      var text = "We caught an error!\n\nThis the Capture+ error information:\n\n";
      text += response;

      // Avoiding multiple alert boxes.
      if (!window.capturePlusExampleAlertShown) {
        window.capturePlusExampleAlertShown = true;
        alert(text);
      }
    });

    search_box.bind('capturePlusCallback.default', function (event, response) {
      $('#edit-result').val(JSON.stringify(response));
    });
  };
})(jQuery, Drupal);
