<?php

/**
 * @file
 * capture_plus theming functions.
 */

define('CAPTURE_PLUS_INCLUDED', TRUE);

/**
 * Theme function for the capture+ field.
 */
function theme_capture_plus_field($element) {
  drupal_add_js(drupal_get_path('module', 'capture_plus') . '/js/capture_plus.js', 'module');

  // Add default key.
  if (empty($element['#capture_plus_settings']['key'])) {
    $element['#capture_plus_settings']['key'] = variable_get('capture_plus_key', '');
  }
  $element['#capture_plus_settings']['name'] = $element['#name'];
  $js_name = str_replace('-', '_', $element['#name']);
  drupal_add_js(array('capturePlus' => array($js_name => $element['#capture_plus_settings'])), 'setting');

  // Add classes.
  $class = array('search-box', 'capture-plus');
  if (isset($element['#attributes']['class'])) {
    $class[] = $element['#attributes']['class'];
  }
  $element['#attributes']['class'] = implode(' ', $class);

  // Return themed textfield.
  return theme('textfield', array(
    '#title' => isset($element['#title']) ? $element['#title'] : t('Address'),
    '#type' => 'textfield',
    '#name' => $element['#name'],
    '#id' => $element['#name'],
    '#attributes' => $element['#attributes'],
    '#value' => '',
    '#parents' => $element['#parents'],
    '#description' => $element['#description'],
    '#autocomplete_path' => FALSE,
  ));
}

/**
 * Preprocess function to include Capture+ external CSS and JS.
 *
 * @param array $variables
 *   Template variables.
 */
function _capture_plus_preprocess_page(&$variables) {
  // Including Capture+ js and css. Using drupal_set_html_head, because
  // drupal_add_js and drupal_add_css don't allow external urls in Drupal 6.
  $js = variable_get('capture_plus_js', 'services.postcodeanywhere.co.uk/js/address-3.20.js');
  $css = variable_get('capture_plus_css', 'services.postcodeanywhere.co.uk/css/address-3.20.css');

  if (variable_get('capture_plus_minified', FALSE)) {
    $js = strtr($js, array('.js' => '.min.js'));
    $css = strtr($css, array('.css' => '.min.css'));
  }

  // Use locally hosted 'stubbed' Capture+ for testing.
  if (variable_get('capture_plus_stub', FALSE)) {
    $css = base_path() . drupal_get_path('module', 'capture_plus') . '/stub/address-3.20.css';
    $js = base_path() . drupal_get_path('module', 'capture_plus') . '/stub/address-3.20.js';
  }

  drupal_set_html_head('<link rel="stylesheet" type="text/css" href="//' . $css . '" />');
  drupal_set_html_head('<script type="text/javascript" src="//' . $js . '"></script>');
  $variables['head'] = drupal_get_html_head();
}
