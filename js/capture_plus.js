/**
 * Initiates the capture plus object.
 */
(function ($, Drupal, pca) {
  'use strict';

  Drupal.behaviors.capturePlusInit = function (context) {
    // Loops over each capture+ field.
    $.each(Drupal.settings.capturePlus, function(id, capturePlusSettings) {
      var element_id = escapeId(capturePlusSettings.name);
      if (element_id && !$('#' + element_id, context).hasClass('capture-plus-processed')) {

        // Use non-escaped id here.
         var fields = [{element: capturePlusSettings.name, field: ""}];
        //var fields = capturePlusSettings.fields;
        capturePlusSettings.control = new pca.Address(fields, capturePlusSettings);

        // Trigger custom event on the html element (textfield).
        capturePlusSettings.control.listen("populate", function(response) {
          $('#' + element_id, context).trigger('capturePlusCallback', [response]);
        });

        // Trigger searches on pasting in content.
        $('#' + element_id, context).bind('paste', function () {
          var element = this;
          window.setTimeout(function () {
            pca.fire(element, 'dblclick');
          }, 0);
        });

        $('#' + element_id, context).addClass('capture-plus-processed');
      }
    });
  };
  function escapeId(id) {
    return id.replace( /(:|\.|\[|\]|,)/g, "\\$1" );
  }
  /**
   * Capture plus default actions.
   *
   * If you want to disable one of these default actions you can use the unbind()
   * function.
   *
   * If you copy a part of this code make sure you replace 'default' by something
   * else in your code (e.g. 'capturePlusCallback.foobar').
   */
  Drupal.behaviors.capturePlusActions = function(context) {
    var search_box = $('input.search-box.capture-plus', context);

    // Capture plus default action.
    search_box.bind('capturePlusCallback.default', function (event, response) {
      $('div.capture-plus.result').remove();
      var html = '<div class="capture-plus result">' + Drupal.theme('capturePlusAddress', response) + '</div>';
      $('input[type=hidden].capture-plus', context).after(html);
    });
  };

  /**
   * Theme function for formatted address.
   *
   * @param response
   *   The Capture+ response.
   * @returns {string}
   *   The formatted address as html.
   */
  Drupal.theme.prototype.capturePlusAddress = function (response) {
    // Replacing new lines with <br />-tags and wrapping it in a paragraph.
    return '<p class="capture-plus address-formatted">' + response.Label.replace(/\n/g, '<br />') + '</p>';
  };

})(jQuery, Drupal, pca);
