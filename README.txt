Capture Plus
https://drupal.org/sandbox/wesleydv/2112333

Provides a Capture+ (by Postcode Anywhere) integration.
This module implements the 'Capture+ web' product from Postcode Anywhere.
You can find more information about it here:
http://www.postcodeanywhere.co.uk/address-capture-software/


Setup
=====

If this is the first time you are using the Capture plus module I suggest that
you enable the Capture plus example module as well. Once done navigate to
/capture-plus-example (You can find a link in the navigation menu). This form
can explain you step by step how to set it up and use it.


Usage
=====

Once the basic setup is done you can use it like any other field in the Form
API. You can see a basic implementation in capture_plus_example.module.

There is some more detailed information about the usage in the
capture_plus_elements function in capture_plus.module.


Extending
=========

There are some default javascript behavior that you can override and extend.
There is plenty of documentation on this in capture_plus.js on this.
You can also have a look at capture_plus_example.js for some more examples.
